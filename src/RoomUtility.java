
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author jpeconi
 * @since 03/27/2016
 *
 * Room Utility class created for assignment 6. This class contains methods that
 * were added on top of the starting code provided from the assignment. These
 * methods will add functionality to the program.
 */
public class RoomUtility {

    /**
     * @author Jamison Peconi
     * @since 03/28/2016
     * @param rooms
     *
     * This method was added for assignment 6. This method contains
     * functionality that will count the number of each type of room and also
     * determine which room is the largest and display it all formatted to the
     * user.
     */
    public static void roomCount(ArrayList<Room> rooms) {
        // Local variables for the roomCount method
        int compRoomCount = 0;
        int biologyLabCount = 0;
        int boardRoomCount = 0;
        int roomCount = 0;
        int specialBoardRoomCount = 0;
        // Variable to be used for the largest room
        int largestNumberSeats = 0;
        // Used String instead of int to be able to handle ties and multiple room numbers
        String largestRooms = "";

        for (Room currentRoom : rooms) {
            // Temp variables to be used for ease of reading and typing in if statements
            int currentNumSeats = currentRoom.getNumberOfSeats();
            int currentRoomNum = currentRoom.getRoomNumber();

            // Statements to satisfy the largest room or rooms
            if (currentNumSeats > largestNumberSeats) {
                largestRooms = Integer.toString(currentRoomNum);
                largestNumberSeats = currentNumSeats;
            } else if (currentNumSeats == largestNumberSeats) {
                largestRooms += "," + currentRoomNum;
            }

            // Code to count the number of rooms
            if (currentRoom instanceof ComputerRoom) {
                compRoomCount++;
            } else if (currentRoom instanceof BiologyLab) {
                biologyLabCount++;
            } else if (currentRoom instanceof BoardRoom) {
                if (currentRoom instanceof SpecialBoardRoom) {
                    specialBoardRoomCount++;
                } else {
                    boardRoomCount++;
                }
            } else {
                roomCount++;
            }
        }

        // Format the output so that the largest room sentence will make
        // sense in the event of multiples. It will pluralize the string if
        // it finds a comma in the list of rooms.
        String largestOutput = "Largest Room: Room #" + largestRooms;
        if (largestRooms.contains(",")) {
            largestOutput = "Largest Rooms are : Room #'s " + largestRooms;
        }
        // Output for the room count summary
        String output = "Room Count Details Report\n"
                + "Room: " + roomCount + "\n"
                + "Computer Room: " + compRoomCount + "\n"
                + "Biology Lab: " + biologyLabCount + "\n"
                + "Board Room: " + boardRoomCount + "\n"
                + "Special Board Room: " + specialBoardRoomCount + "\n"
                + "\n"
                + largestOutput + " with " + largestNumberSeats + " seats";

        System.out.println(output);
    }

    /**
     * @author Jamison Peconi
     * @since 03/28/2016
     * @param rooms
     *
     * This method allows the user to search for a room that match their
     * specific needs. It accepts an ArrayList of Rooms and loops through them
     * only displaying the rooms that match the criteria that the user has
     * entered.
     */
    public static void roomSearch(ArrayList<Room> rooms) {
        Scanner input = new Scanner(System.in);
        String roomType = "";
        String choice;
        boolean finished = false;
        boolean available = false;

        // Prompt the user to select a type of room they are looking for
        do {
            System.out.print("What type of room are you looking for?\n"
                    + "1) Normal Room\n"
                    + "2) Computer Lab\n"
                    + "3) Board Room\n"
                    + "4) Biology lab\n"
                    + "5) Special Board Room\n");
            choice = input.nextLine();

            // Switch on user choice for room type
            // Set the roomtype String to a class name. This string will be used
            // to compare against the class of the current room in the for loop.
            switch (choice) {
                case "1":
                    roomType = "class Room";
                    finished = true;
                    break;
                case "2":
                    roomType = "class ComputerRoom";
                    finished = true;
                    break;
                case "3":
                    roomType = "class BoardRoom";
                    finished = true;
                    break;
                case "4":
                    roomType = "class BiologyLab";
                    finished = true;
                    break;
                case "5":
                    roomType = "class SpecialBoardRoom";
                    finished = true;
                    break;
                default:
                    System.out.println("Invalid option");
            }
        } while (!finished);

        // Prompt the user for the amount of seats required
        System.out.println("How many seats would do you need?");
        int seatsNeeded = input.nextInt();
        input.nextLine();

        System.out.println("\nRooms Available: ");
        // Loops through the rooms
        for (Room currentRoom : rooms) {
            // Compare the current room type to the type the user is looking for
            if (currentRoom.getClass().toString().equals(roomType)) {
                // Compare the number of seats to the amount requested from the user
                if (seatsNeeded <= currentRoom.getNumberOfSeats()) {
                    // Check to see if the room is reserved
                    if (!currentRoom.isReserved()) {
                        // If they found a room that qualifies. Display the details
                        // and set the flag to true
                        System.out.println(currentRoom);
                        System.out.println("");
                        available = true;
                    }
                }
            }
        }

        // Display to the user if no rooms were found
        if (!available) {
            System.out.println("\nSorry! No rooms available.\n");
        }
    }
}
