
import java.util.Scanner;

/**
 *
 * @author jpeconi
 * @since 03/28/2016
 * 
 * Special Board Room class. This class is a child of the board room class. 
 * This class contains a few added attributes required for the assignment that
 * weren't in the board room class. This has been added to the starting code
 * for assignment 6.
 */
public class SpecialBoardRoom extends BoardRoom {
    // Attributes for the Special Board Room
    private int lazyBoyRecliners;
    private boolean coffeeMaker;

    /**
     * @author Jamison Peconi
     * @since 03/28/2016
     * @param roomNumber
     * 
     * This is the custom constructor for the special board room class. This
     * constructor takes the room number and calls the super constructor and
     * passes the room number through to it.
     */
    public SpecialBoardRoom(int roomNumber) {
        super(roomNumber);
    }
    
    /**
     * @author Jamison Peconi
     * @since 03/28/2016
     * 
     * The get room details for user method. This method overrides the get 
     * same method in the super class. However, it makes a call to the super
     * method to ask the initial few questions. It then prompts the user for
     * specific details and sets the attributes for this class.
     */
 
    @Override
    public void getRoomDetailsFromUser() {
        super.getRoomDetailsFromUser();
        Scanner input = new Scanner(System.in);
        System.out.print("Number of Lazy Boy Recliners: ");
        lazyBoyRecliners = input.nextInt();
        input.nextLine();
        System.out.print("Does this room have a coffee maker? (y/n) ");
        coffeeMaker = input.nextLine().equalsIgnoreCase("y");
        
    }

    // Getters
    public int getLazyBoyRecliners() {
        return lazyBoyRecliners;
    }

    public boolean isCoffeeMaker() {
        return coffeeMaker;
    }

    @Override
    public String toString() {
        return super.toString()
                +"\nNumber of Lazy Boy Recliners: "+lazyBoyRecliners
                +"\nHas Coffee Maker: "+coffeeMaker;
           
        
    }
    
    
}
